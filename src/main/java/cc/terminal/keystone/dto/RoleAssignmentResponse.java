package cc.terminal.keystone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author jinxin
 * @author wangchunyang@gmail.com
 */
public class RoleAssignmentResponse {
    @JsonProperty("role_assignments")
    private List<KeystoneRoleAssignment> roleAssignments;

    public List<KeystoneRoleAssignment> getRoleAssignments() {
        return roleAssignments;
    }

    public void setRoleAssignments(List<KeystoneRoleAssignment> roleAssignments) {
        this.roleAssignments = roleAssignments;
    }

    public static class KeystoneRoleAssignment {
        private Scope scope;

        private KeystoneRole role;

        private KeystoneUser user;

        public Scope getScope() {
            return scope;
        }

        public void setScope(Scope scope) {
            this.scope = scope;
        }

        public KeystoneRole getRole() {
            return role;
        }

        public void setRole(KeystoneRole role) {
            this.role = role;
        }

        public KeystoneUser getUser() {
            return user;
        }

        public void setUser(KeystoneUser user) {
            this.user = user;
        }
    }

    public static class Scope {
        private KeystoneProject project;

        public KeystoneProject getProject() {
            return project;
        }

        public void setProject(KeystoneProject project) {
            this.project = project;
        }
    }
}
