package cc.terminal.keystone.dto;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/19.
 */
public class KeystoneProject {
    private String id;
    private String name;
    private boolean enabled = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
