package cc.terminal.keystone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/19.
 */
public class KeystoneRole {
    private String id;
    private String name;
    @JsonProperty("domain_id")
    private String domainId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
