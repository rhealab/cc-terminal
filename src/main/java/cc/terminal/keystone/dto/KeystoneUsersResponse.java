package cc.terminal.keystone.dto;


import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/19.
 */
public class KeystoneUsersResponse {
    List<KeystoneUser> users;

    public List<KeystoneUser> getUsers() {
        return users;
    }

    public void setUsers(List<KeystoneUser> users) {
        this.users = users;
    }
}
