package cc.terminal.keystone.dto;


import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/19.
 */
public class KeystoneProjectsResponse {
    List<KeystoneProject> projects;

    public List<KeystoneProject> getProjects() {
        return projects;
    }
}
