package cc.terminal.keystone.dto;


import cc.terminal.keystone.RoleType;

/**
 * @author wangchunyang@gmail.com
 */
public class RoleAssignment {
    private String userId;
    private String userName;
    private RoleType role;
    private String project;

    public RoleAssignment(String userId, String userName, RoleType role, String project) {
        this.userId = userId;
        this.userName = userName;
        this.role = role;
        this.project = project;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
