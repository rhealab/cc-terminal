package cc.terminal.keystone.dto;


import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/19.
 */
public class KeystoneRoleResponse {
    List<KeystoneRole> roles;

    public List<KeystoneRole> getRoles() {
        return roles;
    }

    public void setRoles(List<KeystoneRole> roles) {
        this.roles = roles;
    }
}
