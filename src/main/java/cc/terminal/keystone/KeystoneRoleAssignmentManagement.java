package cc.terminal.keystone;

import cc.terminal.KeystoneProperties;
import cc.terminal.keystone.dto.RoleAssignment;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/27.
 * @author yanzhixiang modified on 2017-6-7
 */
@Component
public class KeystoneRoleAssignmentManagement {

    private static final Logger logger = LoggerFactory.getLogger(KeystoneRoleAssignmentManagement.class);

    @Autowired
    private KeystoneClient keystoneClient;

    @Autowired
    private KeystoneProperties properties;

    /**
     * cache
     * =================================================================================
     */
    private LoadingCache<String, List<RoleAssignment>> cachedUserRoleAssignments;

    @PostConstruct
    public void init() {
        cachedUserRoleAssignments = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .maximumSize(100)
                .build(CacheLoader.from(this::loadUserRoleAssignments));
    }

    public List<RoleAssignment> getUserRoleAssignmentsCached(String userId) {
        return cachedUserRoleAssignments.getUnchecked(userId);
    }

    /**
     * =================================================================================
     */

    public List<RoleAssignment> getUserRoleAssignments(String userId) {
        if (properties.getCacheEnabled()) {
            return getUserRoleAssignmentsCached(userId);
        } else {
            return loadUserRoleAssignments(userId);
        }
    }

    public boolean isProjectMember(String projectName, String userId) {
        List<RoleAssignment> assignments = getUserRoleAssignments(userId);

        for (RoleAssignment roleAssignment : assignments) {
            if (projectName.equals(roleAssignment.getProject())) {
                return true;
            }
        }

        return false;
    }

    public List<RoleAssignment> loadUserRoleAssignments(String userId) {
        return keystoneClient.loadRoleAssignments(userId, null, null);
    }
}
