package cc.terminal.keystone;

/**
 * @author wangchunyang@gmail.com
 */
public enum RoleType {
    SYS_ADMIN("sys_admin"), NS_ADMIN("site_admin"), DEVELOPER("dev");

    String keystoneName;

    RoleType(String keystoneName) {
        this.keystoneName = keystoneName;
    }

    public String getKeystoneName() {
        return keystoneName;
    }

    public static RoleType fromKeystoneName(String name) {
        for (RoleType roleType : RoleType.values()) {
            if (roleType.getKeystoneName().equals(name))
                return roleType;
        }
        return null;
    }
}