package cc.terminal.keystone;

import cc.terminal.KeystoneProperties;
import cc.terminal.common.FreeMarkerTemplate;
import cc.terminal.error.AppException;
import cc.terminal.error.Err;
import cc.terminal.keystone.dto.KeystoneRole;
import cc.terminal.keystone.dto.KeystoneRoleResponse;
import cc.terminal.keystone.dto.RoleAssignment;
import cc.terminal.keystone.dto.RoleAssignmentResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static cc.terminal.keystone.RoleType.SYS_ADMIN;
import static cc.terminal.utils.OkHttpClientUtils.JSON;
import static cc.terminal.utils.OkHttpClientUtils.getUnsafeOkHttpClient;
import static com.google.common.collect.ImmutableMap.of;


/**
 * @author jinxin
 * @author wangchunyang@gmail.com
 */
@Component
public class KeystoneClient {
    private static final Logger logger = LoggerFactory.getLogger(KeystoneClient.class);
    public final static String DOMAIN_ID = "default";
    private final static String ADMIN_PROJECT_NAME = "admin";

    enum HttpMethod {
        POST, GET, DELETE, PUT
    }

    @Autowired
    private FreeMarkerTemplate freeMarkerTemplate;

    @Autowired
    private KeystoneProperties properties;

    private LoadingCache<String, String> cachedAdminToken;

    @PostConstruct
    public void init() {
        cachedAdminToken = CacheBuilder.newBuilder()
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(CacheLoader.from(this::loadAdminToken));
    }

    private String getAdminTokenCached() {
        String adminTokenId = cachedAdminToken.getUnchecked("admin");
        if (StringUtils.isEmpty(adminTokenId)) {
            adminTokenId = loadAdminToken();
        }
        return adminTokenId;
    }

    String loadAdminToken() {
        String json = freeMarkerTemplate
                .render("keystone_login_request.json",
                        of("username", properties.getAdminUsername(),
                                "domain_id", DOMAIN_ID,
                                "password", properties.getAdminPassword(),
                                "project_name", ADMIN_PROJECT_NAME));

        OkHttpClient httpClient = getUnsafeOkHttpClient();
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder().url(properties.getUrl() + "/auth/tokens").post(body).build();

        try {
            Response response = httpClient.newCall(request).execute();
            String adminToken = response.header("X-Subject-Token");
            if (StringUtils.isEmpty(adminToken)) {
                throw new AppException(Err.KEYSTONE_ERROR, of("msg", "can not login admin"));
            }
            return adminToken;
        } catch (IOException e) {
            throw new AppException(Err.KEYSTONE_ERROR, of("msg", e.getMessage()));
        }
    }

    List<RoleAssignment> loadRoleAssignments(String userId, String projectId, String domainId) {
        StringBuilder subUrl = new StringBuilder();
        subUrl.append("/role_assignments?include_names=true");

        if (userId != null) {
            subUrl.append("&user.id=").append(userId);
        }

        if (projectId != null) {
            subUrl.append("&scope.project.id=").append(projectId);
        }

        if (domainId != null) {
            subUrl.append("&scope.domain.id=").append(domainId);
        }

        try {
            RoleAssignmentResponse assignmentResponse = execute(subUrl.toString(), HttpMethod.GET, null,
                    RoleAssignmentResponse.class);

            List<RoleAssignmentResponse.KeystoneRoleAssignment> keystoneRoleAssignments = assignmentResponse.getRoleAssignments();
            List<RoleAssignment> assignments = new ArrayList<>();

            //compose assignments and filter SYS_ADMIN (long long ago, api server need a namespace have a sys admin for use)
            for (RoleAssignmentResponse.KeystoneRoleAssignment keystoneRoleAssignment : keystoneRoleAssignments) {
                RoleAssignment assignment = composeRoleAssignment(keystoneRoleAssignment);
                RoleType role = assignment.getRole();
                if (role != null
                        && !(role == SYS_ADMIN && !assignment.getProject().isEmpty())
                        && !assignment.getUserId().isEmpty()) {
                    assignments.add(assignment);
                }
            }

            return assignments;
        } catch (IOException e) {
            logger.error("Failed to get role assignments for user {}", userId, e);
            throw new AppException(Err.KEYSTONE_ERROR, of("msg", e.getMessage()));
        }
    }

    RoleAssignment composeRoleAssignment(RoleAssignmentResponse.KeystoneRoleAssignment assignment) {
        String userId = "";
        String userName = "";
        if (assignment.getUser() != null) {
            userId = assignment.getUser().getId();
            userName = assignment.getUser().getName();
        }

        String project = "";
        if (assignment.getScope() != null && assignment.getScope().getProject() != null) {
            project = assignment.getScope().getProject().getName();
        }
        RoleType role = RoleType.fromKeystoneName(assignment.getRole().getName());
        return new RoleAssignment(userId, userName, role, project);
    }

    List<KeystoneRole> loadKeystoneRoles() {
        String subUrl = "/roles";
        try {
            KeystoneRoleResponse roleResponse = execute(subUrl, HttpMethod.GET, null, KeystoneRoleResponse.class);
            return roleResponse.getRoles();
        } catch (IOException e) {
            logger.error("Failed to get roles {}", e);
            throw new AppException(Err.KEYSTONE_ERROR, of("msg", e.getMessage()));
        }
    }

    private <T> T execute(String subUrl,
                          HttpMethod requestMethod,
                          RequestBody requestBody,
                          Class<T> responseClass) throws IOException {
        Request request = new Request.Builder()
                .header("X-Auth-Token", getAdminTokenCached())
                .url(properties.getUrl() + subUrl)
                .method(requestMethod.name(), requestBody)
                .build();
        OkHttpClient httpClient = getUnsafeOkHttpClient();

        Response response = httpClient.newCall(request).execute();
        if (response.isSuccessful()) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            T value = mapper.readValue(response.body().string(), responseClass);
            if (response.body() != null) {
                response.close();
            }
            return value;
        }

        if (response.code() == HttpStatus.UNAUTHORIZED.value()) {
            cachedAdminToken.refresh("admin");
        }

        if (response.body() != null) {
            response.close();
        }

        logger.error("Failed to exec url: {}", subUrl);
        throw new AppException(Err.KEYSTONE_ERROR, of("msg", response.message()));
    }
}
