package cc.terminal;

import cc.terminal.utils.OkHttpClientUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.internal.ws.RealWebSocket;
import okio.ByteString;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/24.
 */
@Component
public class FrontendWebSocketHandler extends AbstractWebSocketHandler {
    private static Logger logger = LoggerFactory.getLogger(FrontendWebSocketHandler.class);
    private static final long MAX_QUEUE_SIZE = 16 * 1024 * 1024;
    private static Map<String, WebSocket> webSocketMap = new ConcurrentHashMap<>();

    @Autowired
    private KubernetesProperties kubernetesProperties;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        Map<String, Object> attributes = session.getAttributes();
        String namespaceName = attributes.get("namespaceName").toString();
        String podName = attributes.get("podName").toString();
        String composedQueryString = attributes.get("composedQueryString").toString();

        String url = getPodExecUrl(namespaceName, podName, composedQueryString);

        OkHttpClient client = OkHttpClientUtils.getClientForWebSocket();
        Request request = new Request.Builder()
            .get()
            .addHeader("Authorization", kubernetesProperties.getAuthorizationToken())
            .url(url)
            .build();
        CountDownLatch countDownLatch = new CountDownLatch(1);

        logger.info("start connect to k8s, session: {}", session.getId());
        WebSocket webSocket = client.newWebSocket(request, new K8sWebSocketListener(session, countDownLatch));
        webSocketMap.put(session.getId(), webSocket);

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info("frontend web socket: {} established", session.getId());
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        WebSocket webSocket = webSocketMap.get(session.getId());
        webSocket.send("\u0000" + message.getPayload());
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
        WebSocket webSocket = webSocketMap.get(session.getId());
        byte[] data = message.getPayload().array();
        if (webSocket != null) {
            byte[] toSendData = new byte[data.length + 1];
            toSendData[0] = 0;
            System.arraycopy(data, 0, toSendData, 1, data.length);
            if (webSocket.queueSize() + data.length > this.MAX_QUEUE_SIZE) {
                while (true) {
                    this.sleep(2);
                    if (webSocket.queueSize() + data.length < this.MAX_QUEUE_SIZE) {
                        break;
                    }
                }
            }

            webSocket.send(ByteString.of(toSendData));
        }
    }

    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        WebSocket webSocket = webSocketMap.get(session.getId());
        webSocket.send(ByteString.of(message.getPayload()));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        logger.info("frontend web socket - {} error - {}", session.getId(), exception == null ? "" : exception.getMessage());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        logger.info("frontend web socket: {} closed {}", session == null ? "" : session.getId(), status.getReason());
        if (session != null) {
            WebSocket webSocket = webSocketMap.remove(session.getId());
            webSocket.close(status.getCode(), status.getReason());
        }
    }

    private String getPodExecUrl(String namespaceName,
                                 String podName,
                                 String composedQueryString) {
        String url = kubernetesProperties.getApiUrl() + "namespaces/" + namespaceName
            + "/pods/" + podName + "/exec?1=1";

        if (!StringUtils.isEmpty(composedQueryString)) {
            url += composedQueryString;
        }

        return url;
    }

    private void sleep(int mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
