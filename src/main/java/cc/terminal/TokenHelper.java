package cc.terminal;

import cc.terminal.token.Token;
import cc.terminal.token.TokenManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/24.
 */

@Component
public class TokenHelper {
    @Autowired
    private TokenManager tokenManager;

    public String getJwtTokenString(HttpServletRequest request) {

        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            token = request.getHeader("Sec-WebSocket-Protocol");
        }

        if (StringUtils.isEmpty(token)) {
            token = request.getParameter("Authorization");
        }

        if (StringUtils.isEmpty(token)) {
            return "";
        }

        return token.substring("Bearer ".length());
    }

    public Token getToken(HttpServletRequest request) {
        String jwtTokenString = getJwtTokenString(request);
        return tokenManager.decodeToken(jwtTokenString);
    }

    public boolean isAuthorized(HttpServletRequest request) {
        Token token = getToken(request);
        return isAuthorized(token);
    }

    public boolean isAuthorized(Token token) {
        return token.isValid()
                && token.getExpirationTime() != null
                && token.getExpirationTime().after(new Date());
    }

    public String getUserId(HttpServletRequest request) {
        return getToken(request).getUserId();
    }
}
