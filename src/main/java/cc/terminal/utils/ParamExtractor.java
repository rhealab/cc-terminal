package cc.terminal.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author xzy
 */
public class ParamExtractor {
    public static String extractNamespace(String path) {
        String namespace = StringUtils.substringBetween(path, "kubernetes/namespaces/", "/");
        if (StringUtils.isBlank(namespace)) {
            namespace = StringUtils.substringAfter(path, "kubernetes/namespaces/");
        }

        return namespace;
    }

    public static String extractPod(String path) {
        String pod = StringUtils.substringBetween(path, "/pods/", "?");
        if (StringUtils.isBlank(pod)) {
            pod = StringUtils.substringAfter(path, "/pods/");
        }

        return pod;
    }

    public static String extractContainer(String queryString) {
        String container = StringUtils.substringBetween(queryString, "container=", "&");
        if (StringUtils.isBlank(container)) {
            container = StringUtils.substringAfter(queryString, "container=");
        }

        return container;
    }
}
