package cc.terminal;

import cc.terminal.utils.OkHttpClientUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author wangchunyang@gmail.com
 */
@Component
@ConfigurationProperties(prefix = "terminal")
public class TerminalProperties {
    private boolean authEnabled;
    private long httpConnectTimeoutMillis;
    private long httpReadTimeoutMillis;
    private long httpWriteTimeoutMillis;
    private long pingIntervalMillis;

    @PostConstruct
    private void init() {
        OkHttpClientUtils.setConnectTimeoutMillis(getHttpConnectTimeoutMillis());
        OkHttpClientUtils.setReadTimeoutMillis(getHttpReadTimeoutMillis());
        OkHttpClientUtils.setWriteTimeoutMillis(getHttpWriteTimeoutMillis());
        OkHttpClientUtils.setPingIntervalMillis(getPingIntervalMillis());
    }

    public boolean isAuthEnabled() {
        return authEnabled;
    }

    public void setAuthEnabled(boolean authEnabled) {
        this.authEnabled = authEnabled;
    }

    public long getHttpConnectTimeoutMillis() {
        return httpConnectTimeoutMillis;
    }

    public void setHttpConnectTimeoutMillis(long httpConnectTimeoutMillis) {
        this.httpConnectTimeoutMillis = httpConnectTimeoutMillis;
    }

    public long getHttpReadTimeoutMillis() {
        return httpReadTimeoutMillis;
    }

    public void setHttpReadTimeoutMillis(long httpReadTimeoutMillis) {
        this.httpReadTimeoutMillis = httpReadTimeoutMillis;
    }

    public long getHttpWriteTimeoutMillis() {
        return httpWriteTimeoutMillis;
    }

    public void setHttpWriteTimeoutMillis(long httpWriteTimeoutMillis) {
        this.httpWriteTimeoutMillis = httpWriteTimeoutMillis;
    }

    public long getPingIntervalMillis() {
        return pingIntervalMillis;
    }

    public void setPingIntervalMillis(long pingIntervalMillis) {
        this.pingIntervalMillis = pingIntervalMillis;
    }

    @Override
    public String toString() {
        return "TerminalProperties{" +
                "httpConnectTimeoutMillis=" + httpConnectTimeoutMillis +
                ", httpReadTimeoutMillis=" + httpReadTimeoutMillis +
                ", httpWriteTimeoutMillis=" + httpWriteTimeoutMillis +
                ", pingIntervalMillis=" + pingIntervalMillis +
                '}';
    }
}

