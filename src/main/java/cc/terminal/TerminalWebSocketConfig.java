package cc.terminal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/24.
 */
@Configuration
@EnableWebMvc
@EnableWebSocket
@ComponentScan
public class TerminalWebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

    @Autowired
    private FrontendWebSocketHandler frontendWebSocketHandler;

    @Autowired
    private FrontendWebSocketHandshakeInterceptor frontendWebSocketHandshakeInterceptor;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(frontendWebSocketHandler, "/api/v1/kubernetes/namespaces/{namespaceName}/pods/{podName}")
                .setAllowedOrigins("*")
                .addInterceptors(frontendWebSocketHandshakeInterceptor);
    }
}