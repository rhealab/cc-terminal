package cc.terminal;

import cc.terminal.keystone.KeystoneRoleAssignmentManagement;
import cc.terminal.token.Token;
import cc.terminal.utils.ParamExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/18.
 */
@Component
public class FrontendWebSocketHandshakeInterceptor implements HandshakeInterceptor {
    private static Logger logger = LoggerFactory.getLogger(FrontendWebSocketHandshakeInterceptor.class);

    @Autowired
    private TokenHelper tokenHelper;

    @Autowired
    private KeystoneRoleAssignmentManagement roleAssignmentManagement;

    @Autowired
    private TerminalProperties properties;

    @Override
    public boolean beforeHandshake(ServerHttpRequest request,
                                   ServerHttpResponse response,
                                   WebSocketHandler wsHandler,
                                   Map<String, Object> attributes) {
        ServletServerHttpRequest serverHttpRequest = (ServletServerHttpRequest) request;
        String path = request.getURI().getPath();
        String namespaceName = ParamExtractor.extractNamespace(path);
        String podName = ParamExtractor.extractPod(path);
        if (properties.isAuthEnabled()) {
            Token token = tokenHelper.getToken(serverHttpRequest.getServletRequest());
            logger.info("user - {} entered", token.getUserId());

            if (!tokenHelper.isAuthorized(token) && StringUtils.isEmpty(token.getUserId())) {
                logger.info("request - {} unauthorized", request.getURI().getPath());
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return false;
            }

            if (!roleAssignmentManagement.isProjectMember(namespaceName, token.getUserId())) {
                logger.info("user - {} has no access on namespace - {}", token.getUserId(), namespaceName);
                response.setStatusCode(HttpStatus.FORBIDDEN);
                return false;
            }
            logger.info("user - {} is access on namespace - {} and start to connect to pod - {}",
                    token.getUserId(), namespaceName, podName);
        }

        attributes.put("namespaceName", namespaceName);
        attributes.put("podName", podName);
        String queryString = composeQueryString(serverHttpRequest);
        attributes.put("composedQueryString", queryString);

        logger.info("queryString - {}", queryString);
        return true;
    }

    private String composeQueryString(ServletServerHttpRequest serverHttpRequest) {
        StringBuilder composedQueryString = new StringBuilder();
        Map<String, String[]> parameterMap = new HashMap<>(serverHttpRequest.getServletRequest().getParameterMap());
        parameterMap.remove("Authorization");

        String[] commands = parameterMap.remove("command");
        if (commands == null) {
            composedQueryString.append("&command=/bin/sh&command=-i&tty=true&stdin=true&stdout=true&stderr=true");
        } else {
            for (String command : commands) {
                composedQueryString.append("&command=").append(command);
            }
        }

        String[] containers = parameterMap.remove("container");
        if (containers != null) {
            for (String container : containers) {
                composedQueryString.append("&container=").append(container);
            }
        }
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String key = entry.getKey();
            String[] values = entry.getValue();
            for (String value : values) {
                composedQueryString.append("&").append(key).append("=").append(value);
            }
        }

        return composedQueryString.toString();
    }

    @Override
    public void afterHandshake(ServerHttpRequest request,
                               ServerHttpResponse response,
                               WebSocketHandler wsHandler,
                               Exception exception) {
    }
}
