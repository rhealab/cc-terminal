package cc.terminal;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.websocket.CloseReason.CloseCodes;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import static com.google.common.base.Charsets.UTF_8;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/19.
 */
public class K8sWebSocketListener extends WebSocketListener {
    private static Logger logger = LoggerFactory.getLogger(K8sWebSocketListener.class);

    private WebSocketSession frontendSession;

    private CountDownLatch countDownLatch;

    public K8sWebSocketListener(WebSocketSession frontendSession, CountDownLatch countDownLatch) {
        this.frontendSession = frontendSession;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        logger.info("k8s web socket open, related frontend session - {}", frontendSession.getId());
        countDownLatch.countDown();
        if (!frontendSession.isOpen()) {
            webSocket.close(CloseCodes.NORMAL_CLOSURE.getCode(), "frontend session is closed");
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        if (frontendSession.isOpen()) {
            try {
                frontendSession.sendMessage(new TextMessage(text));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        if (frontendSession.isOpen()) {
            try {
                if (frontendSession.getUri().getQuery().contains("tar")) {
                    frontendSession.sendMessage(new BinaryMessage(bytes.toByteArray()));
                } else {
                    frontendSession.sendMessage(new TextMessage(bytes.string(UTF_8)));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("k8s web socket closed, related frontend session - {}", frontendSession.getId());
        closeFrontendSession(code, reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        String reason;
        if (response != null && response.body() != null) {
            try {
                reason = response.body().string();
            } catch (IOException e) {
                reason = t.getMessage();
            }
        } else {
            reason = t.getMessage();
        }
        logger.info("k8s web socket failure, related frontend session - {}, reason - {}",
                frontendSession.getId(), reason);
        countDownLatch.countDown();

        closeFrontendSession(CloseCodes.UNEXPECTED_CONDITION.getCode(), reason);
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(code, reason);
    }

    private void closeFrontendSession(int code, String reason) {
        if (countDownLatch.getCount() > 0) {
            countDownLatch.countDown();
        }

        if (frontendSession.isOpen()) {
            try {
                frontendSession.close(new CloseStatus(code, reason));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}