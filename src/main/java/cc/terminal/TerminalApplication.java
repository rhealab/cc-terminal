package cc.terminal;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

/**
 * a web socket server for k8s exec
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/19.
 */
@SpringBootApplication
public class TerminalApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(TerminalApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(TerminalApplication.class, args);
    }

    @Override
    public void run(String... args) {
        logger.debug("Starting BackendApplication ...");
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
        factory.addAdditionalTomcatConnectors(createConnector());
        return factory;
    }

    private Connector createConnector() {
        Connector connector = new Connector(TomcatEmbeddedServletContainerFactory.DEFAULT_PROTOCOL);
        connector.setPort(8818);
        return connector;
    }
}