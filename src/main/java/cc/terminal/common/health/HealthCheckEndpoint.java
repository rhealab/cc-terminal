package cc.terminal.common.health;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@RequestMapping("api/v1")
@Controller
@EnableAutoConfiguration
public class HealthCheckEndpoint {

    @RequestMapping("health")
    @ResponseBody
    public Health health() {
        Health health = new Health();
        health.setName("cc-terminal");
        health.setVersion("1.0.0");
        health.setDeveloper("cc team");
        health.setMessage("Welcome to Container Management Console!");
        health.setStatus("OK");
        health.setTimestamp(new Date());
        return health;
    }
}
