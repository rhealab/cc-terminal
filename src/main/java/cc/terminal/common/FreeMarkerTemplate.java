package cc.terminal.common;

import cc.terminal.error.AppException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class FreeMarkerTemplate {
    private Configuration cfg;

    @PostConstruct
    public void init() {
        cfg = new Configuration(Configuration.VERSION_2_3_25);
        cfg.setClassLoaderForTemplateLoading(this.getClass().getClassLoader(), "freemarker");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    public String render(String templateFile, Object model) {
        try {
            Template template = cfg.getTemplate(templateFile);
            StringWriter writer = new StringWriter();
            template.process(model, writer);
            String str = writer.toString();
            writer.close();
            return str;
        } catch (IOException | TemplateException e) {
            throw new AppException("Failed to render FreeMarker template: " + templateFile, e);
        }
    }
}