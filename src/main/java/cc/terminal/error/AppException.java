package cc.terminal.error;

import com.google.common.collect.ImmutableMap;
import org.springframework.core.NestedRuntimeException;

import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
public class AppException extends NestedRuntimeException {
    private Err error;
    private Map<String, Object> payload;

    public AppException() {
        super("");
    }

    public AppException(String msg) {
        super(msg);
        this.error = Err.SERVER_ERROR;
        this.payload = ImmutableMap.of();
    }

    public AppException(String msg, Throwable cause) {
        super(msg, cause);
        this.error = Err.SERVER_ERROR;
        this.payload = ImmutableMap.of();
    }

    public AppException(Err error) {
        this(error, ImmutableMap.of());
    }

    public AppException(Err error, Map<String, Object> payload) {
        super(error.toString());
        this.error = error;
        this.payload = payload;
    }

    public Err getError() {
        return error;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

}
