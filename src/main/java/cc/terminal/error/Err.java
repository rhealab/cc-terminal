package cc.terminal.error;


/**
 * @author wangchunyang@gmail.com
 */
public enum Err {
    OK(20000, "succeed"),

    // Bad Request: common
    PROPERTY_INVALID(40000, "The format of property is not valid"),
    LEGACY_NAMESPACE(40099, "The namespace is legacy and quota doesn't exist"),
    HTTP_ERROR(40008, "The generic http error"),

    // Bad Request: Memory and CPU
    MEMORY_OVER_LIMITS(40001, "The memory requests can not larger than limits"),
    CPU_OVER_LIMITS(40002, "The cpu requests can not larger than limits"),

    // Bad Request: Storage
    STORAGE_OVER_QUOTA(40003, "The storage quota is over"),
    STORAGE_QUOTA_TOO_LOW(40004, "The storage quota should be greater than 10% of current usage"),
    NAMESPACE_STORAGE_QUOTA_TOO_LOW(40005, "The namespace storage quota should be larger than reserved size"),
    STORAGE_IN_USE(40006, "The storage is in use, can not be deleted"),
    STORAGE_TYPE_NOT_SUPPORTED(40007, "The namespace doesn't support the storage type"),
    NAMESPACE_LIMIT_QUOTA_TOO_LOW(40009, "The namespace quota should be larger than reserved size"),
    NAMESPACE_REQUEST_QUOTA_TOO_LOW(40010, "The namespace quota should be larger than reserved size"),
    STORAGE_DEPLOYMENT_NOT_MATCH(40011, "The used storage only can be access by the same name deployment"),
    HOSTPATH_STORAGE_REPLICA_LARGE(40012, "Only replica is less than 1's hostpath storage can changed unshared to false"),
    RESOURCE_STATUS_NOT_CORRECT(40013, "resource status not support current operation"),

    // Bad Request: Memory and Cpu plus used can not larger than quota
    MEMORY_REQUEST_OVER_QUOTA(40014, "memory request plus used memory request can not larger than quota"),
    MEMORY_LIMITS_OVER_QUOTA(40015, "memory limits plus used memory limits can not larger than quota"),
    CPU_REQUEST_OVER_QUOTA(40016, "cpu request plus used cpu request can not larger than quota"),
    CPU_LIMITS_OVER_QUOTA(40017, "cpu limits plus used cpu limits can not larger than quota"),

    // Namespace create bad request
    NAMESPACE_MEMORY_REQUEST_TOO_HIGH(40018, "namespace memory requests plus used memory requests can not larger than cluster memory sum"),
    NAMESPACE_CPU_REQUEST_TOO_HIGH(40019, "namespace cpu requests plus used cpu requests can not larger than cluster cpu sum"),
    NAMESPACE_HOSTPATH_TOO_HIGH(40020, "namespace host path size plus used host path size can not larger than cluster local storage size"),
    NAMESPACE_RBD_TOO_HIGH(40021, "namespace rbd size plus used rbd size can not larger than cluster rbd size"),

    // Auth
    USER_NOT_AUTHENTICATED(40100, "The user has not login in"),
    USER_NOT_AUTHORIZED(40300, "The user has no permission"),

    // NOT FOUND
    RESOURCE_NOT_FOUND(40400, "The resource is not found"),

    // CONFLICTS
    RESOURCE_CONFLICTS(40900, "The resource already exists"),
    K8S_RESOURCE_CONFLICT(40901, "The resource conflicts in Kubernetes"),

    // Server Error
    SERVER_ERROR(50000, "Internal server error"),
    K8S_ERROR(50001, "Kubernetes server error"),
    KEYSTONE_ERROR(50002, "Keystone server error");

    private final int code;
    private final String message;

    Err(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Err{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    public static Err getErrorByCode(int code) {
        Err[] errs = Err.values();
        for (Err err : errs) {
            if (err.getCode() == code) {
                return err;
            }
        }
        return SERVER_ERROR;
    }
}

