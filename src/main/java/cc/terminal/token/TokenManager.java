package cc.terminal.token;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.time.Instant;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class TokenManager {
    private static final Logger logger = LoggerFactory.getLogger(TokenManager.class);
    public static final String USERNAME = "u";
    public static final String USER_ID = "uid";
    public static final String WRAPPED_TOKEN = "wtk";

    @Autowired
    private RSAKeyPairReader rsaKeyPairReader;

    private JWSVerifier verifier;

    @PostConstruct
    public void init() {
        verifier = createVerifier();
    }

    public boolean validateToken(String jwtToken) {
        try {
            SignedJWT signedJWT = SignedJWT.parse(jwtToken);
            boolean verified = signedJWT.verify(verifier);
            if (verified) {
                Date expirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
                return expirationTime.toInstant().isAfter(Instant.now());
            }
            return false;
        } catch (ParseException | JOSEException e) {
            logger.error("Failed to verify jwtToken: {}", jwtToken, e);
        }
        return false;
    }

    private JWSVerifier createVerifier() {
        return createRSAVerifier();
    }

    private JWSVerifier createRSAVerifier() {
        return new RSASSAVerifier(rsaKeyPairReader.readPublicKey("keys/public_key.der"));
    }

    public Token decodeToken(String jwtToken) {
        Token token = new Token();
        try {
            SignedJWT signedJWT = SignedJWT.parse(jwtToken);
            boolean verified = signedJWT.verify(verifier);
            token.setValid(verified);
            if (verified) {
                JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
                token.setUsername((String) claimsSet.getClaim(USERNAME));
                token.setUserId((String) claimsSet.getClaim(USER_ID));
                token.setWrappedToken((String) claimsSet.getClaim(WRAPPED_TOKEN));
                token.setExpirationTime(claimsSet.getExpirationTime());
            }
        } catch (ParseException | JOSEException e) {
            logger.error("Failed to verify token: {}", jwtToken, e);
        }
        return token;
    }
}
