package cc.terminal;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Base64;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/19.
 */
@Component
@ConfigurationProperties(prefix = "kubernetes")
public class KubernetesProperties {
    private String masterHost;
    private String masterPort;
    private boolean masterSecure;
    private String adminUsername;
    private String adminPassword;
    private String filterNamespaces;

    private String apiUrl;
    private String authorizationToken;

    @PostConstruct
    private void init() {
        String masterUrl = masterSecure ? "wss://" : "ws://";
        masterUrl += masterHost + ":" + masterPort + "/";

        apiUrl = masterUrl + "api/v1/";

        String str = adminUsername + ":" + adminPassword;
        authorizationToken = "Basic " + Base64.getEncoder().encodeToString(str.getBytes());
    }

    public String getMasterHost() {
        return masterHost;
    }

    public void setMasterHost(String masterHost) {
        this.masterHost = masterHost;
    }

    public String getMasterPort() {
        return masterPort;
    }

    public void setMasterPort(String masterPort) {
        this.masterPort = masterPort;
    }

    public boolean isMasterSecure() {
        return masterSecure;
    }

    public void setMasterSecure(boolean masterSecure) {
        this.masterSecure = masterSecure;
    }

    public String getAdminUsername() {
        return adminUsername;
    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getFilterNamespaces() {
        return filterNamespaces;
    }

    public void setFilterNamespaces(String filterNamespaces) {
        this.filterNamespaces = filterNamespaces;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthorizationToken() {
        return authorizationToken;
    }

    public void setAuthorizationToken(String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }
}
